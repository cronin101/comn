/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class SocketSender {
	
	protected DatagramSocket sendSocket;
	protected int payloadnum = 1;
	protected int port;
	protected String filename;
	protected String host;
	protected int timeout;
	protected FileSender output;
	protected int retransmissions;
	protected long startTime;
	public SocketSender(String[] args) throws Exception{
		this.host = args[0];
		this.port = Integer.parseInt(args[1]);
		this.filename = args[2];
	}
	
	//sets up the socket and runs the sendFileTo method forever
	public void activate(boolean istimeout) throws Exception{
		try {
			sendSocket = new DatagramSocket(port+1);
		} catch (SocketException e) {
		System.out.println("SocketSender could not listen on port: "+port+1);
		System.exit(-1);
		}
		System.out.println("SocketSender listening on port: "+(port+1));
		if (istimeout){
			sendSocket.setSoTimeout(timeout);
		}
		output = null;
		output = new FileSender(filename);
		startTime = System.currentTimeMillis();
		retransmissions = 0;
		while (true){
			sendFileTo(port,output);
		}
	}
	
	//used for closing the socket cleanly after sending has finished
	public void closeSocket(){
		System.out.println("SocketSender closed.");
		//calculates metrics
		long stopTime = System.currentTimeMillis();
		float timeTaken = stopTime-startTime;
		float rate = ((float)output.datasize/1024)/(timeTaken/1000);
		System.out.printf("Sent %dB in %fms at a rate of %f kB/s with %d retransmissions\n",output.datasize,timeTaken,rate,retransmissions);
		sendSocket.close();
	}
	
	//looped forever, controls then entire sending of the file
	public void sendFileTo(int port, FileSender output) throws InterruptedException, IOException{
		if (payloadnum > output.numPayloads()){
			System.out.println("Finished sending file, will now exit.");
			closeSocket();
			System.exit(1);
		}
		//packet constructed
		byte[] dataToSend = output.getFullPayloadN(payloadnum);
		DatagramPacket sendPacket = new DatagramPacket(dataToSend,dataToSend.length,InetAddress.getByName(host),port);
		try {
			sendSocket.send(sendPacket);
		} catch (IOException e) {
			System.out.println("Port unreachable");
			System.exit(-1);
		}
		payloadnum++;
	}
}
