/* Aaron Cronin s0925570 */

import java.io.IOException;

//all sending logic is in the superclass, this is the default socket implementation for sending
public class Sender1 extends SocketSender{
	
	public Sender1(String[] args) throws Exception {
		super(args);
	}
	
	public void sendFileTo(int port, FileSender output) throws InterruptedException, IOException{
		super.sendFileTo(port,output);
		//sleep added to increase reliability
		Thread.sleep(20);
	}
	
	public static void main(String[] args) throws Exception{
		
		if (args.length < 3){
			System.out.println("USAGE: java Sender1 IP port filename");
			System.exit(-1);
		}
		
		Sender1 sender = new Sender1(args);
		sender.activate(false);
	}
	
}
