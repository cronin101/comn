/* Aaron Cronin s0925570 */

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class FileReceiver {	
	
	private int receiveddata;
	private String strFilePath;
	
	//Stream to write binary data to
	private BufferedOutputStream output;
	
	//Keeps track of packet order
	public int lastwrote=0;
	
	public boolean hasFinished;
	
	//Time that Stream was opened
	long startTime;
	//Time that EOF was seen
	long finishTime;
	
	public FileReceiver(String filename) throws FileNotFoundException{
		hasFinished = false;
		output = getOutputStream(filename);
		lastwrote = 0;
		startTime = System.currentTimeMillis();
	}
	
	public int getValue(byte[] data){
		return (((data[0]<<8) & 0x0000FF00)|(data[1] & 0x000000FF));
	}
	
	
	private void insertPayloadData(int paynum, byte[] payloaddata) throws IOException{
		receiveddata+=payloaddata.length;
		lastwrote++;
		//System.out.println(lastwrote+" Wrote packet "+paynum);
		output.write(payloaddata);
	}
	
	
	private BufferedOutputStream getOutputStream(String filename) throws FileNotFoundException{
		strFilePath = filename;
		FileOutputStream fos = new FileOutputStream(strFilePath);
		return new BufferedOutputStream(fos);
		
	}
	
	public void receivePayload(byte[] payload) throws IOException{
		byte[] intheader = new byte[2];
		
		//integer header is extracted
		intheader[0] = payload[0];
		intheader[1] = payload[1];
		int payloadnum = getValue(intheader);
		boolean EOF = (payload[2] == 1);
		
		//payload data is extracted
		byte[] payloaddata = new byte[payload.length-3];
		for (int i = 0; i < (payload.length-3); i++){
			payloaddata[i] = payload[3+i];
		}

		//payload data in written to output file
		insertPayloadData(payloadnum,payloaddata);
		
		if (EOF){
			finishTime = System.currentTimeMillis();
			System.out.println("< EOF recognised.");
			System.out.printf("< SAVING FILE\n");
			saveFileToDisk();
			System.out.printf("< FILE SAVED\n");
			System.out.printf("< Downloaded %d in %d ms at a rate of %f.\n", receiveddata, finishTime-startTime, ((float)receiveddata/1024)/(((float)finishTime-startTime)/1000));
			hasFinished = true;
			try {
				FileSender util = new FileSender(strFilePath);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private void saveFileToDisk() throws IOException {
		output.close();
		
	}
}
