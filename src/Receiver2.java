/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;


public class Receiver2 extends SocketReceiver {
	private int token = 1;
	public Receiver2(String[] args) throws IOException {
		super(args);
	}
	
	public byte[] constructIntHeader(int paynum){
		byte[] headerint = new byte[2];
		headerint[0] = (byte) (paynum >>> 8);
		headerint[1] = (byte) paynum;
		return headerint;
	}
	
	public void closeSocket(){
		for (int i = 0; i < 1000; i++){
			//being nice and sending acks
			byte[] ack = constructIntHeader((token));
			DatagramPacket ackpacket;
			try {
				ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
				try {
					super.receiveSocket.send(ackpacket);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		System.out.println("SocketReciever closed.");
		receiveSocket.close();
	}
	
	
	
	public static void main(String[] args) throws IOException{
		if (args.length<2){
			System.out.println("USAGE: java Receiver2 port filename");
			System.exit(-1);
		}
		Receiver2 receiver = new Receiver2(args);
		receiver.activate();
	}
}
