/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;


public class Sender3 extends SocketSender{
	private int windowsize;
	private int basenum;
	
	public Sender3 (String[] args) throws Exception{
		super(args);
		timeout = Integer.parseInt(args[3]);
		windowsize = Integer.parseInt(args[4]);
		basenum = 1;
	}
	
	public void sendFileTo(int port, FileSender output) throws InterruptedException, IOException{
		//exit when done
		if (basenum == output.numPayloads()){
			closeSocket();
			System.exit(1);
		}
		
		//reduce window when not enough packets remain
		if (basenum + windowsize >= output.numPayloads()){
			windowsize = (output.numPayloads()+1) - basenum;
		}
		//send all data inside the window
		for (int i = 0; i < windowsize; i++){
			byte[] packetdata = output.getFullPayloadN(basenum+i);
			DatagramPacket outpacket = new DatagramPacket(packetdata,packetdata.length,InetAddress.getByName(host),port);
			try {
				sendSocket.send(outpacket);
			} catch (IOException e) {
				System.out.println("Port unreachable");
				System.exit(-1);
			}
		}
		//try and receive all the acks
		for (int i = 0; i < windowsize; i++){
			byte[] ack = new byte[2];
			DatagramPacket ackPacket = new DatagramPacket(ack,ack.length);
			//get an ack
			try{
				super.sendSocket.receive(ackPacket);
				int acknum = getValue(ack);
				//increment base of window
				if (acknum+1 > basenum){
					basenum = acknum+1;
				}
			} catch (SocketTimeoutException e){
				//no more acks so next iteration
				return;
			}
		}
	}
	
	public int getValue(byte[] data){
		return (((data[0]<<8) & 0x0000FF00)|(data[1] & 0x000000FF));
	}
	
	public static void main(String[] args) throws Exception{
		if (args.length < 5){
			System.out.println("USAGE: java Sender3 IP port filename timeout windowsize");
			System.exit(-1);
		}
		Sender3 sender = new Sender3(args);
		sender.activate(true);
	}
}
