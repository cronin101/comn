/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.util.HashMap;


public class Receiver4 extends SocketReceiver {
	private int token = 1;
	private HashMap<Integer,byte[]> bytebuffer;
	private int windowsize;
	
	public Receiver4(String[] args) throws IOException {
		super(args);
		windowsize = Integer.parseInt(args[2]);
		bytebuffer = new HashMap<Integer,byte[]>();
	}
	public byte[] constructIntHeader(int paynum){
		byte[] headerint = new byte[2];
		headerint[0] = (byte) (paynum >>> 8);
		headerint[1] = (byte) paynum;
		return headerint;
	}
	
	public void closeSocket(){
		for (int i = 0; i < 1000; i++){
			//being nice and sending acks
			byte[] ack = constructIntHeader((token));
			DatagramPacket ackpacket;
			try {
				ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
				try {
					super.receiveSocket.send(ackpacket);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (SocketException e1) {
			}
			
		}
		System.out.println("SocketReciever closed.");
		receiveSocket.close();
	}
	
	//looped to handle incoming data
	public void receiveFile(FileReceiver input) throws IOException{
		byte[] receiveBuffer = new byte[PAYLOAD_SIZE]; 
		//consume useful data (the needed packet) from the buffer as many times as possible
		for (int i = 0; i < bytebuffer.size(); i++){		
			if (bytebuffer.containsKey(token)){
				input.receivePayload(bytebuffer.get(token));
				bytebuffer.remove(token);
				token++;
			}
		}
		
		if (input.hasFinished){
			closeSocket();
			System.exit(1);
		}
		
		//receive packet
		receivePacket = new DatagramPacket(receiveBuffer,receiveBuffer.length);
		receiveSocket.receive(receivePacket);
		receiveData = new byte[receivePacket.getLength()]; 
		System.arraycopy(receiveBuffer, 0,receiveData,0, receivePacket.getLength());
		
		//interpret header
		int numrec = input.getValue(receiveData);
		//ack packets
		if ((numrec < token + windowsize - 1)&&(!bytebuffer.containsKey(numrec))){
			byte[] ack = constructIntHeader(numrec);
			DatagramPacket ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
			super.receiveSocket.send(ackpacket);
			//add to buffer if it is relevant
			if (numrec >= token){
				bytebuffer.put(numrec, receiveData);
			}
		}
	}
	
	public static void main(String[] args) throws IOException{
		if (args.length<3){
			System.out.println("USAGE: java Receiver4 port filename windowsize");
			System.exit(-1);
		}
		Receiver4 receiver = new Receiver4(args);
		receiver.activate();
	}
}
