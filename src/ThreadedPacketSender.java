/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;


public class ThreadedPacketSender extends Thread {
	private DatagramPacket packet;
	private DatagramSocket sendSocket;
	private int timeout;
	
	public ThreadedPacketSender(DatagramPacket packet, DatagramSocket sendSocket, int timeout){
		this.packet = packet;
		this.sendSocket = sendSocket;
		this.timeout = timeout;

	}
	// Thread repeatedly sends provided packet using socket provided from .start() to .interrupt();
	public void run(){
		while (true){
			try {
				sendSocket.send(packet);
			} catch (IOException e1) {
			}
			//interrupt used to kill the thread.
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				return;
			}
		}
	}
	
}
