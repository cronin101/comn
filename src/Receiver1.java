/* Aaron Cronin s0925570 */


import java.io.IOException;


public class Receiver1 extends SocketReceiver {

	public Receiver1(String[] args) throws IOException {
		super(args);
	}
	
	
	
	public static void main(String[] args) throws IOException{
		if (args.length<2){
			System.out.println("USAGE: java Receiver1 port filename");
			System.exit(-1);
		}
		Receiver1 receiver = new Receiver1(args);
		receiver.activate();
	}
}
