/* Aaron Cronin s0925570 */


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;



public class FileSender {
	
	//Size of each payload
	public final static int PAYLOAD_SIZE = 1024;
	
	//Size of the header of each payload (2 byte numerical id and a EOF flag)
	public final static int HEADER_SIZE = 3;

	//Space left in payload for actual data
	public final static int PAYLOAD_DATASIZE = PAYLOAD_SIZE - HEADER_SIZE;
	
	//Binary data being stored
	private byte[] data;
	public int datasize;
	protected int sentbits = 0;
	
	
	//CHECKSUM CODE LIFTED FROM http://www.rgagnon.com/javadetails/java-0416.html
	public byte[] createChecksum(String filename) throws Exception {
	       InputStream fis =  new FileInputStream(filename);

	       byte[] buffer = new byte[1024];
	       MessageDigest complete = MessageDigest.getInstance("MD5");
	       int numRead;

	       do {
	           numRead = fis.read(buffer);
	           if (numRead > 0) {
	               complete.update(buffer, 0, numRead);
	           }
	       } while (numRead != -1);

	       fis.close();
	       return complete.digest();
	   }

	   public  String getMD5Checksum(String filename) throws Exception {
	       byte[] b = createChecksum(filename);
	       String result = "";

	       for (int i=0; i < b.length; i++) {
	           result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
	       }
	       return result;
	   }
	
	 //END CHECKSUM CODE
	
	private byte[] getFileBytes(File inputfile) throws IOException{
		byte[] bytes = new byte[(int) inputfile.length()];
		FileInputStream input = new FileInputStream(inputfile);
		input.read(bytes);
		input.close();
		return bytes;
	}
	
	public int numPayloads(){
		return (int) Math.ceil(((double)datasize / PAYLOAD_DATASIZE));
	}
	
	public FileSender(String filename) throws Exception{
		File file = new File(filename);
		String checksum = getMD5Checksum(filename);
		System.out.printf("Reading '%s' into byte array, checksum is %s\n",filename,checksum);
		data = getFileBytes(file);
		datasize = data.length;
	}
	
	public byte[] getPayload(int startbyte, int size){
		byte[] payload = new byte[size];
		for (int i = 0; i<size;i++){
			payload[i] = data[startbyte+i];
		}
		return payload;
	}
	
	public byte[] getPayloadN(int n){
		int startbyte = (n-1)*PAYLOAD_DATASIZE;
		int thispayloadsize;
		if (n != numPayloads()){
			thispayloadsize = PAYLOAD_DATASIZE;
		} else {
			thispayloadsize = datasize-startbyte;
		}
		return getPayload(startbyte,thispayloadsize);
	}
	
	public byte[] constructFullPayload(byte[] intheader, byte[] eofflag, byte[] payloaddata ){
		byte[] fullPayload = new byte[payloaddata.length+3];
		fullPayload[0] = intheader[0];
		fullPayload[1] = intheader[1];
		fullPayload[2] = eofflag[0];
		for (int i = 0; i < payloaddata.length; i++){
			fullPayload[3+i] = payloaddata[i];
		}
		return fullPayload;
	}
	
	public byte[] constructIntHeader(int paynum){
		byte[] headerint = new byte[2];
		headerint[0] = (byte) (paynum >>> 8);
		headerint[1] = (byte) paynum;
		return headerint;
	}
	
	public byte[] constructEOFFlag(boolean isEOF){
		byte[] flag = new byte[1];
		if (isEOF){
			flag[0] = (byte) 0x01;
		} else {
			flag[0] = (byte) 0x00;
		}
		return flag;
	}
	
	public byte[] getFullPayloadN(int paynum){
		//System.out.println("Sending packet "+paynum+" of "+numPayloads());
		
		//Create unsigned int header that stores numerical id of payload
		byte[] headerint = constructIntHeader(paynum);
		
		//Create single byte header that is blank unless EOF
		//System.out.println(paynum == numPayloads());
		byte[] EOFflag = constructEOFFlag(paynum == numPayloads());
		
		//Actual data in the payload
		byte[] payloadData = getPayloadN(paynum);
		
		//Everything is combined to make the full payload
		return constructFullPayload(headerint, EOFflag, payloadData);
	}
}
