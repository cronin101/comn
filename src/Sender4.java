/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;


public class Sender4 extends SocketSender {
	private int windowsize;
	private int windowstart;
	private int nextpacket;
	private int highestacked;
	
	private HashMap<Integer,ThreadedPacketSender> window;
	
	public Sender4(String[] args)throws Exception {
		super(args);
		timeout = Integer.parseInt(args[3]);
		windowsize = Integer.parseInt(args[4]);
		windowstart = 1;
		highestacked = 0;
		window = new HashMap<Integer,ThreadedPacketSender>();
		
	}
	
	//set up the socket
	public void activate(boolean istimeout) throws Exception{
		try {
			sendSocket = new DatagramSocket(port+1);
		} catch (SocketException e) {
		System.out.println("SocketSender could not listen on port: "+port+1);
		System.exit(-1);
		}
		System.out.println("SocketSender listening on port: "+(port+1));
		if (istimeout){
			sendSocket.setSoTimeout(timeout);
		}
		output = null;
		output = new FileSender(filename);
		startTime = System.currentTimeMillis();
		
		//set up the initial window
		for (int i = 0; i < windowsize; i++){
			byte[] packetdata = output.getFullPayloadN(windowstart+i);
			DatagramPacket outpacket = new DatagramPacket(packetdata,packetdata.length,InetAddress.getByName(host),port);
			window.put(windowstart+i, new ThreadedPacketSender(outpacket,super.sendSocket, timeout));
			//start the threaded sending
			window.get(windowstart+i).start();
		}
		
		while (true){
			sendFileTo(port,output);
		}
	}
	
	public int getValue(byte[] data){
		return (((data[0]<<8) & 0x0000FF00)|(data[1] & 0x000000FF));
	}
	
	public void sendFileTo(int port, FileSender output) throws InterruptedException, IOException{
		//get an ack if it exists
		byte[] ack = new byte[2];
		DatagramPacket ackPacket = new DatagramPacket(ack,ack.length);
		try{
			super.sendSocket.receive(ackPacket);
			int acknum = getValue(ack);
			if (acknum > highestacked){
				highestacked = acknum;
			}
			//protects against trying to remove something that has already been removed
			if (window.containsKey(acknum)){
				//stop trying to send that packet
				window.get(acknum).interrupt();
				//remove it from the window (so that the window can be moved)
				window.remove(acknum);
			}
		} catch (SocketTimeoutException e){
			//continue
		}
		

		if ((highestacked == output.numPayloads())&&(window.size() == 0)){
			closeSocket();
			System.exit(1);
		}
		
		if (window.size() > 0){
			//get first unacked packet in window
			Vector<Integer> v = new Vector<Integer>(window.keySet());
			Collections.sort(v);
			//if the window needs to be expanded
			if ((v.firstElement().hashCode() > windowstart)){
				nextpacket = (windowstart)+windowsize;
				windowstart = v.firstElement().hashCode();
				//expand the window with new threaded senders
				for(int i = nextpacket; i < windowstart + windowsize; i++){
					if (i > output.numPayloads()){
						return;
					}
					byte[] packetdata = output.getFullPayloadN(i);
					DatagramPacket outpacket = new DatagramPacket(packetdata,packetdata.length,InetAddress.getByName(host),port);
					window.put(i, new ThreadedPacketSender(outpacket,super.sendSocket, timeout));
					//start the threaded sending
					window.get(i).start();
				}
			}
		} else {
			windowstart = highestacked-1;
			nextpacket = highestacked + 1;
			//expand the window with new threaded senders
			for(int i = nextpacket; i < windowstart + windowsize; i++){
				if (i > output.numPayloads()){
					return;
				}
				byte[] packetdata = output.getFullPayloadN(i);
				DatagramPacket outpacket = new DatagramPacket(packetdata,packetdata.length,InetAddress.getByName(host),port);
				window.put(i, new ThreadedPacketSender(outpacket,super.sendSocket, timeout));
				//start the threaded sending
				window.get(i).start();
			}
		}
	}
	
	public static void main(String[] args) throws Exception{
		if (args.length < 5){
			System.out.println("USAGE: java Sender4 IP port filename timeout windowsize");
			System.exit(-1);
		}
		Sender4 sender = new Sender4(args);
		sender.activate(true);
	}
	

}
