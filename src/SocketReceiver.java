/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class SocketReceiver {
	protected DatagramSocket receiveSocket;
	public final int PAYLOAD_SIZE = 1024;
	protected int port;
	private String filename;
	protected byte[] receiveData;
	protected DatagramPacket receivePacket;
	
	public SocketReceiver(String[] args) throws IOException{
		this.port = Integer.parseInt(args[0]);
		this.filename = args[1];
	}
	
	//sets up the receiving socket and then loops the receiving code forever
	public void activate() throws IOException{
		try {
			receiveSocket = new DatagramSocket(port);
		} catch (SocketException e) {
			System.out.println("SocketReciever could not listen on port: "+port);
			System.exit(-1);
		}
		System.out.println("SocketReciever listening on port: "+port);
		FileReceiver input = new FileReceiver(filename);
		while(true){
			receiveFile(input);
		}
	}
	
	//looped constantly to handle incoming data and process it
	public void receiveFile(FileReceiver input) throws IOException{
		byte[] receiveBuffer = new byte[PAYLOAD_SIZE];
		if (input.hasFinished){
			System.out.println("Receiving finished, will now exit.");
			closeSocket();
			System.exit(1);
		}	
		receivePacket = new DatagramPacket(receiveBuffer,receiveBuffer.length);
		receiveSocket.receive(receivePacket);
		receiveData = new byte[receivePacket.getLength()]; 
		System.arraycopy(receiveBuffer, 0,receiveData,0, receivePacket.getLength());
		input.receivePayload(receiveData);
	}
	
	
	public void closeSocket(){
		System.out.println("SocketReciever closed.");
		receiveSocket.close();
	}
}
