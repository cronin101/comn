/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;


public class Sender2 extends SocketSender {
		
	public Sender2(String[] args) throws Exception {
		super(args);
		timeout = Integer.parseInt(args[3]);
	}
	
	//looped forever to send the file using Stop-and-Wait
	public void sendFileTo(int port, FileSender output) throws IOException {
		if (payloadnum > output.numPayloads()){
			System.out.println("Finished sending file, will now exit.");
			closeSocket();
			System.exit(1);
		}
		//packet created
		byte[] dataToSend = output.getFullPayloadN(payloadnum);
		//integer header is replaced with either 1 or 0
		dataToSend[0] = output.constructIntHeader(payloadnum%2)[0];
		dataToSend[1] = output.constructIntHeader(payloadnum%2)[1];

		DatagramPacket sendPacket = new DatagramPacket(dataToSend,dataToSend.length,InetAddress.getByName(host),port);
		try {
			sendSocket.send(sendPacket);
		} catch (IOException e) {
			System.out.println("Port unreachable");
			System.exit(-1);
		}
		
		//ack to be received
		byte[] ack = new byte[2];
		DatagramPacket ackPacket = new DatagramPacket(ack,ack.length);
		
		try{
			super.sendSocket.receive(ackPacket);
		} catch (SocketTimeoutException e){
			// Return without incrementing payloadnum to re-send packet.
			super.retransmissions++;
			return;
		}
		if (getValue(ack) == (payloadnum%2)){
			// Correct ack was received so the next packet should be sent.
			payloadnum++;
		} else {
			// Return without incrementing payloadnum to re-send packet.
			super.retransmissions++;
			return;
		}
	}
	
	//used to decode packet header
	public int getValue(byte[] data){
		return (((data[0]<<8) & 0x0000FF00)|(data[1] & 0x000000FF));
	}
	
	public static void main(String[] args) throws Exception{
		if (args.length < 4){
			System.out.println("USAGE: java Sender2 IP port filename timeout");
			System.exit(-1);
		}
		Sender2 sender = new Sender2(args);
		sender.activate(true);

	}

}
