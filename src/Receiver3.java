/* Aaron Cronin s0925570 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;


public class Receiver3 extends SocketReceiver {
	
	private int token = 1;
	
	public Receiver3(String[] args) throws IOException{
		super(args);
	}
	
	public byte[] constructIntHeader(int paynum){
		byte[] headerint = new byte[2];
		headerint[0] = (byte) (paynum >>> 8);
		headerint[1] = (byte) paynum;
		return headerint;
	}
	
	public void closeSocket(){
		for (int i = 0; i < 1000; i++){
			//being nice and sending acks
			byte[] ack = constructIntHeader((token));
			DatagramPacket ackpacket;
			try {
				ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
				try {
					super.receiveSocket.send(ackpacket);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		System.out.println("SocketReciever closed.");
		receiveSocket.close();
	}
	
	public void receiveFile(FileReceiver input) throws IOException{
		byte[] receiveBuffer = new byte[PAYLOAD_SIZE]; 
		if (input.hasFinished){
			System.out.println("Receiving finished, will now exit.");
			closeSocket();
			System.exit(1);
		}	
		receivePacket = new DatagramPacket(receiveBuffer,receiveBuffer.length);
		receiveSocket.receive(receivePacket);
		receiveData = new byte[receivePacket.getLength()]; 
		System.arraycopy(receiveBuffer, 0,receiveData,0, receivePacket.getLength());
		
		if (input.getValue(receiveData)==(token)){
			input.receivePayload(receiveData);
			//send ack of last succeeded packet
			byte[] ack = constructIntHeader(token);
			DatagramPacket ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
			super.receiveSocket.send(ackpacket);
			
			//increment token
			this.token++;
		} else {
		//send ack of last succeeded packet
		byte[] ack = constructIntHeader(token-1);
		DatagramPacket ackpacket = new DatagramPacket(ack, ack.length, receivePacket.getSocketAddress());
		super.receiveSocket.send(ackpacket);
		}
	}
	
	public static void main(String[] args) throws IOException{
		if (args.length<2){
			System.out.println("USAGE: java Receiver3 port filename");
			System.exit(-1);
		}
		Receiver3 receiver = new Receiver3(args);
		receiver.activate();
	}
}
